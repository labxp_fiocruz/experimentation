# SITH - Experimentation 
SITH corresponde ao nome dado a esse projeto, e tem como significado "Scientific Information Topics Highlighter". Assim, este repositório apresenta análise de dados onde o objetivo é extrair tópicos envolvendo tecnologias emergentes de uma base de notícias na área da saúde para uma pesquisadores da Fiocruz. Toda a documentação envolvendo a arquitetura e visão de produto encontram-se no repositório [documentation](https://gitlab.com/labxp_fiocruz/documentation).

[<img src="images/sith.png" width="500" title="SITH Logo" align="center">]()

## Visão
Todo a motivação e contextualização sobre o desenvolvimento e propósito deste projeto encontram-se no documento de [visão](https://gitlab.com/labxp_fiocruz/documentation/-/blob/dev/Vision/vision.md). Porém, em resumo, este projeto teve início na disciplina de Laboratório de Métodos Ágeis fornecida pelo instituto IME da Universidade de São Paulo (USP), sendo o grupo de desenvolvimento formado por alunos da mesma. O projeto tem por objetivo desenvolver uma solução tecnológica que permita o processamento de linguagem natural de notícias da área da saúde, a fim de extrair de maneira organizada e de fácil interpretação informações sobre possíveis tecnologias/áreas/projetos/pesquisas emergentes a fim de servir como material de estudo pelos funcionários da Fundação Oswaldo Cruz (Fiocruz), nossos clientes.

## Como Utilizar
Toda a arquitetura do projeto foi descrita no [documento de arquitetura](https://gitlab.com/labxp_fiocruz/documentation/-/blob/dev/architecture/architecture.md) e recomendamos a leitura para melhor entendimento do fluxo de dados do projeto. Assim, no diretório src encontra-se o arquivo SITHAPI.py que corresponde à interface de programação que acessará o conjunto de dados de treinamento que está na pasta data e chama os outros módulos que resultaram no produto da análise.

## Para testar
Na pasta test pode-se encontrar os seguintes arquivos, cada um deles contendo um objetivo de teste diferente:

- test_Cleaner.py - teste de unidade para a biblioteca Cleaner.py;
- test_Splitter.py - teste de unidade para a biblioteca Splitter.py;
- test_PreProcessor.py - teste de unidade para a biblioteca PreProcessor.py;
- test_utils.py - teste de unidade para o utils.py;
- test_Tfidf.py - teste de unidade para o Tfidf.py;

## Quer contribuir?
Gostou do SITH e gostaria de contribuir com ele? Acesse o [guia de contruição](https://gitlab.com/labxp_fiocruz/documentation/-/blob/dev/CONTRIBUTING.md) para saber como pode contribuir com o projeto.

## Autores
- Daniel Silva Lopes da Costa
- Daniella Fernanda Cisterna Melo
- Gabriela Jie Han
- Isaque Alves de Lima
- Leonardo Martinez Ikeda
- Victor Senoguchi Borges

## License
For open source projects, say how it is licensed.

## Status do Projeto
O projeto encontra-se em estágios finais de desenvolvimento.

## Criando e Utilizando a lib
Fizemos o upload da lib deste projeto no repositório TestPyPi, caso queira consultar o passo a passo basta acessar a [documentação do PyPA](https://packaging.python.org/en/latest/tutorials/packaging-projects/#packaging-python-projects)

Assim, para utilizar essa [lib do SITH](https://test.pypi.org/project/SITH/) basta importá-la através do comando pip:
`pip install -i https://test.pypi.org/simple/ SITH` 
