import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../../src')
from utils import remove_special_characters, clean_broke_lines


def test_remove_special_caracter():
    acceptable_special = """
    ,_().- '"/:

    """

    dirty_string = acceptable_special + "\\"
    assert acceptable_special == remove_special_characters(dirty_string)

def test_clean_broke_line():
    clean_broke_lines()
