import sys
sys.path.insert(1, '../../src')

from Cleaner import Cleaner
import pandas as pd
import numpy as np

def test_value_duplicated():
    dirty_data_duplicated = pd.read_csv('../data/dirty_data_duplicated.csv')
    expected_data_duplicated = pd.read_csv('../data/expected_data_duplicated.csv')

    _cleaner = Cleaner(dirty_data_duplicated)

    assert all(expected_data_duplicated == _cleaner.clean_dataset(dirty_data_duplicated).reset_index(drop = True))


def test_value_nan_text():
    dirty_data_nan_text = pd.read_csv('../data/dirty_data_nan_text.csv')
    expected_data_nan_text = pd.read_csv('../data/expected_data_nan_text.csv')

    _cleaner = Cleaner(dirty_data_nan_text)
    assert all(expected_data_nan_text == _cleaner.clean_dataset(dirty_data_nan_text).reset_index(drop = True))


def test_remove_JS():
    dirty_data_javascript = pd.read_csv('../data/dirty_data_javascript.csv')
    expected_data_javascript = pd.read_csv('../data/expected_data_javascript.csv')

    _cleaner = Cleaner(dirty_data_javascript)
    assert all(expected_data_javascript == _cleaner.clean_dataset(dirty_data_javascript).reset_index(drop = True))

def test_remove_outliers_by_percentile():
    dirty_data = pd.read_csv('../data/clean_data.csv')
    len_dirty = len(dirty_data)

    _cleaner = Cleaner(dirty_data)
    for i in np.arange(0.005, 0.1, 0.005):
        for j in np.arange(0.005, 0.1, 0.005):
            clean_data = _cleaner.remove_outliers_by_percentile(dirty_data, lower = i, upper = j)
            len_clean_expected = np.floor(np.floor(len_dirty-len_dirty*i)-len_dirty*j)
            assert abs(len_clean_expected - len(clean_data)) <= 2
