import sys
sys.path.insert(1, '../../src')

import cleaning_split
import pandas as pd
import numpy as np


# test broken line
def test_broken_line():
    dirty_data_broken_line = '../data/dirty_data_broken_line.csv'
    expected_data_broken_line = pd.read_csv('../data/expected_data_broken_line.csv')

    assert all(expected_data_broken_line == cleaning_split.get_dataframe(dirty_data_broken_line))


def test_value_duplicated():
    dirty_data_duplicated = pd.read_csv('../data/dirty_data_duplicated.csv')
    expected_data_duplicated = pd.read_csv('../data/expected_data_duplicated.csv')

    assert all(expected_data_duplicated == cleaning_split.clean_dataset(dirty_data_duplicated).reset_index(drop = True))


def test_value_nan_text():
    dirty_data_nan_text = pd.read_csv('../data/dirty_data_nan_text.csv')
    expected_data_nan_text = pd.read_csv('../data/expected_data_nan_text.csv')

    assert all(expected_data_nan_text == cleaning_split.clean_dataset(dirty_data_nan_text).reset_index(drop = True))


def test_remove_JS():
    dirty_data_javascript = pd.read_csv('../data/dirty_data_javascript.csv')
    expected_data_javascript = pd.read_csv('../data/expected_data_javascript.csv')

    assert all(expected_data_javascript == cleaning_split.clean_dataset(dirty_data_javascript).reset_index(drop = True))

def test_remove_outliers_by_percentile():
    dirty_data = pd.read_csv('../data/clean_data.csv')
    len_dirty = len(dirty_data)
    for i in np.arange(0.5, 10, 0.5):
        for j in np.arange(0.5, 10, 0.5):
            clean_data = cleaning_split.remove_outliers_by_percentile(dirty_data, lower = i, upper = j)
            len_clean_expected = np.floor(np.floor(len_dirty-len_dirty*i/100)-len_dirty*j/100)
            assert abs(len_clean_expected - len(clean_data)) <= 2


def test_clean_dataset():
    dirty_data = pd.read_csv('../data/dirty_data.csv')
    expected_data = pd.read_csv('../data/clean_data.csv')

    assert all(expected_data == cleaning_split.clean_dataset(dirty_data).reset_index(drop = True))
