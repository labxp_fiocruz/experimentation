import sys
sys.path.insert(1, '../../src')

from DataLoader import DataLoader
import pandas as pd


def test_broken_line():
    dirty_data_broken_line = '../data/dirty_data_broken_line.csv'
    expected_data_broken_line = pd.read_csv('../data/expected_data_broken_line.csv')

    _dataLoader = DataLoader()
    assert all(
        expected_data_broken_line == _dataLoader.get_dataframe(dirty_data_broken_line)
    )
