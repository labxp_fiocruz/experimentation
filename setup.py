import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="SITH",
    version="0.0.1",
    author="Isaque Alves",
    author_email="isaquealvesdl@gmail.com",
    description="SITH corresponde ao nome dado a esse projeto e tem como significado Scientific Information Topics Highlighter.",
    url="https://gitlab.com/labxp_fiocruz/experimentation/",
    project_urls={
        "Bug Tracker": "https://gitlab.com/labxp_fiocruz/experimentation/-/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.6",
)
