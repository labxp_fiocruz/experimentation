import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer

def get_tf_idf(list_of_tokens, ngram_min=1, ngram_max=1):
    vectorizer = TfidfVectorizer(preprocessor = ' '.join, ngram_range=(ngram_min,ngram_max))
    vectors = vectorizer.fit_transform(list_of_tokens)
    feature_names = vectorizer.get_feature_names()
    vectors_list = vectors.todense().tolist()
    return vectors_list, feature_names

def get_tf_idf_matrix(vectors_list, feature_names):
    return pd.DataFrame(vectors_list, columns=feature_names)

def get_most_significant_tokens(vectors_list, feature_names, number_of_tokens=10):
    most_significant_tokens_matrix = []
    for vector in vectors_list:
        tokens = []
        for _, feature in sorted(zip(vector, feature_names), reverse=True)[:number_of_tokens]:
            tokens.append(feature)
        most_significant_tokens_matrix.append(tokens)
    return most_significant_tokens_matrix
