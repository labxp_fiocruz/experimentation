from utils import remove_special_characters, clean_broke_lines, default_data_filename, default_train_filename, default_test_filename
import pandas as pd
from sklearn.model_selection import train_test_split

class Splitter:
    def __init__(self, dataframe=pd.DataFrame()):
        self.dataframe = dataframe

    def data_split(self, dataframe, train_filename=default_train_filename,
                   test_filename=default_test_filename):
        X_train, X_test = train_test_split(dataframe, test_size = .3,
                                           stratify = dataframe[['crawler']])
        X_test.to_csv(test_filename, index=False)
        X_train.to_csv(train_filename, index=False)
