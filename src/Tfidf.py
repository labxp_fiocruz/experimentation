import pandas as pd
import json
from sklearn.feature_extraction.text import TfidfVectorizer

class Tfidf:
    def __init__(self, list_of_tokens=pd.Series()):
        self.list_of_tokens = list_of_tokens
        self.ngram_min=1
        self.ngram_max=1
        self.number_of_tokens=10
        self.min_df=1
        self.max_features=None

    def get_tf_idf(self, list_of_tokens, ngram_min=1, ngram_max=1, min_df=1, max_features=None):
        vectorizer = TfidfVectorizer(
            tokenizer=(lambda x: x),
            preprocessor=(lambda x: x),
            ngram_range=(ngram_min,ngram_max),
            min_df=min_df,
            max_features=max_features
        )
        vectors = vectorizer.fit_transform(list_of_tokens)
        feature_names = vectorizer.get_feature_names()
        vectors_list = vectors.todense().tolist()
        return vectors_list, feature_names

    def get_tf_idf_matrix(self, vectors_list, feature_names):
        return pd.DataFrame(vectors_list, columns=feature_names)

    def get_most_significant_tokens(self, vectors_list, feature_names, text_ids, number_of_tokens=10,
                                json_output_file=None):
        list_of_ids_and_tokens = []
        for (vector, text_id) in zip(vectors_list, text_ids):
            tokens = []
            for _, feature in sorted(zip(vector, feature_names), reverse=True)[:number_of_tokens]:
                tokens.append(feature)
            list_of_ids_and_tokens.append((text_id, tokens))
        if json_output_file:
            keys = ['text_id', 'most_significant_tokens']
            data = [dict(zip(keys, id_and_token)) for id_and_token in list_of_ids_and_tokens]
            with open(json_output_file, 'w') as outfile:
                json.dump(data, outfile)
        return list_of_ids_and_tokens
