import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import regex as re
from sklearn.model_selection import train_test_split
from utils import remove_special_characters, clean_broke_lines, default_data_filename


def get_dataframe(data_filename=default_data_filename):
    was_cleaned = clean_broke_lines(data_filename)
    dataframe = pd.read_csv(data_filename)

    if not was_cleaned:
        dataframe = mitigate_dirty_data(dataframe)

    return dataframe

def mitigate_dirty_data(dataframe):
    colunas = list(dataframe.columns)
    clean_dataset = dataframe.copy()
    last_value = clean_dataset['_id'][2813]

    for i in range(1,len(colunas)):
        last_value = dataframe[colunas[i]][2813]
        clean_dataset[colunas[i]][2813] = dataframe[colunas[i-1]][2813]

    clean_dataset["_id"][2813] = dataframe["_id"][2812]
    clean_dataset["title"][2813] = dataframe["title"][2812]

    return clean_dataset

def remove_outliers_by_percentile(dataframe, lower = .5, upper = .5):
    if len(dataframe) > 20:
        dataframe['text_len'] = dataframe.text.apply(lambda x: len(x))
        dataframe = dataframe[dataframe.text_len > dataframe.text_len.quantile(lower/100)]
        dataframe = dataframe[dataframe.text_len < dataframe.text_len.quantile((100-upper)/100)]
        dataframe = dataframe.drop(columns=['text_len'])

    return dataframe

def clean_dataset(dataframe):
    #Excluindo os que são vídeos e a linha contendo o texto totalmente em japonês
    index_video_data = dataframe[dataframe['text'].isna()].index.tolist()
    dataframe = dataframe.drop(index_video_data)

    #Excluindo os duplicados
    dataframe = dataframe.sort_values(by="date")
    dataframe.drop_duplicates(subset=['text'], keep='first', inplace = True)

    #Limpando dos textos ruídos contendo sintaxe de javascript
    java_noise_pattern = re.compile(r'(!function\().*(\(\);)', re.UNICODE)
    dataframe.text = dataframe.text.apply(lambda x: java_noise_pattern.sub('', x))

    #Limpando dos textos caracteres japoneses
    japanese_characters_pattern = re.compile(
        r'([{\u3000-\u303F}{一-龯}\p{IsHira}\p{IsKatakana}]+)',
        re.UNICODE
    )
    dataframe.text = dataframe.text.apply(lambda x: japanese_characters_pattern.sub(' ', x))

    #Limpando outliers em relação ao tamanho dos textos
    dataframe = remove_outliers_by_percentile(dataframe)

    return dataframe

def data_split(dataframe):
    #Separando os dados em 70-30
    X_train, X_teste = train_test_split(dataframe, test_size = .3, stratify = dataframe[['crawler']], random_state=1)
    X_teste.to_csv('../data/teste.csv', index=False)
    X_train.to_csv('../data/train.csv', index=False)


def data_preprocessing():
    dataframe = get_dataframe(default_data_filename)
    dataframe = clean_dataset(dataframe)

    data_split(dataframe)

if __name__ == '__main__':
    data_preprocessing()
