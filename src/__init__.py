from Splitter import Splitter
from DataLoader import DataLoader
from Cleaner import Cleaner
from PreProcessor import PreProcessor
from Tfidf import Tfidf
import pandas as pd
from utils import (
    default_data_filename,
    default_train_filename,
    default_test_filename,
    default_output_filename
)

class SITHAPI:
    def __init__(self):
        self._dataLoader = DataLoader()
        self._cleaner = Cleaner(dataframe=pd.DataFrame())
        self._splitter = Splitter(dataframe=pd.DataFrame())
        self._preProcessor = PreProcessor(texts = pd.Series(), list_of_tokens = pd.Series())
        self._tfidf = Tfidf(list_of_tokens = pd.Series())

    def run_all(self):
        dataframe = self.get_dataframe(data_filename=default_data_filename)
        self.split(dataframe)

        train = self.get_dataframe(data_filename=default_train_filename)
        train = self.clean_dataframe(train)
        texts = train.text
        train['tokens'] = self.tokenize_multiple_texts(texts, include_stopwords=False,
                                                           include_punctuation=False,
                                                           include_like_num=False,
                                                           include_only_ascii=True,
                                                           lemmatize=True,
                                                           lowercase=True)

        vectors_list, feature_names = self.get_tf_idf(train.tokens, ngram_min=1, ngram_max=3,
                                                      min_df=.025, max_features=12000)
        ids = train._id
        return self.get_most_significant_tokens(vectors_list, feature_names, ids=ids,
                                                number_of_tokens=10)

    def get_dataframe(self, data_filename):
        return self._dataLoader.get_dataframe(data_filename=data_filename)

    def clean_dataframe(self, dataframe, text_size_lower_percentile=.005,
                        text_size_upper_percentile=.005):
        return self._cleaner.clean_dataset(
            dataframe, text_size_lower_percentile, text_size_upper_percentile
        )

    def split(self, dataframe, train_filename=default_train_filename,
              test_filename=default_test_filename):
        self._splitter.data_split(dataframe, train_filename, test_filename)

    def tokenize_single_text(self, text, include_stopwords=False, include_punctuation=False,
                             include_like_num=False, include_only_ascii=True, include_pos=[]):
        return self._preProcessor.tokenize_single_text(text, include_stopwords, include_punctuation,
                                                       include_like_num, include_only_ascii,
                                                       include_pos=[])

    def tokenize_multiple_texts(self, texts, include_stopwords=False, include_punctuation=False,
                                include_like_num=False, include_only_ascii=True, include_pos=[],
                                lemmatize=True, lowercase=True):
        tokens = self._preProcessor.tokenize_multiple_texts(texts, include_stopwords,
                                                            include_punctuation, include_like_num,
                                                            include_only_ascii, include_pos=[])
        if lemmatize:
            tokens = self._preProcessor.lemmatize_multiple_texts(tokens)
        else:
            tokens = self._preProcessor.list_of_tokens_as_string(tokens)

        if lowercase:
            tokens = self._preProcessor.lowercase_list_of_tokens(tokens)

        return tokens

    def get_tf_idf(self, list_of_tokens, ngram_min=1, ngram_max=1, min_df=1, max_features=None):
        return self._tfidf.get_tf_idf(list_of_tokens, ngram_min, ngram_max, min_df, max_features)

    def get_tf_idf_matrix(self, vectors_list, feature_names):
        return self._tfidf.get_tf_idf_matrix(vectors_list, feature_names)

    def get_most_significant_tokens(self, vectors_list, feature_names, ids, number_of_tokens=10,
                                    json_output_file=default_output_filename):
        return self._tfidf.get_most_significant_tokens(vectors_list, feature_names, ids,
                                                       number_of_tokens, json_output_file)
