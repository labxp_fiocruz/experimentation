import pandas as pd
import numpy as np
import regex as re
from utils import remove_special_characters, clean_broke_lines, default_data_filename

class Cleaner:

    def __init__(self, dataframe=pd.DataFrame()):
        self.dataframe = dataframe

    def remove_outliers_by_percentile(self, dataframe, lower = .005, upper = .005):
        if len(dataframe) > 20:
            dataframe['text_len'] = dataframe.text.apply(lambda x: len(x))
            dataframe = dataframe[dataframe.text_len > dataframe.text_len.quantile(lower)]
            dataframe = dataframe[dataframe.text_len < dataframe.text_len.quantile((1-upper)/1)]
            dataframe = dataframe.drop(columns=['text_len'])

        return dataframe

    def clean_dataset(self, dataframe, text_size_lower_percentile=0, text_size_upper_percentile=0):
        index_video_data = dataframe[dataframe['text'].isna()].index.tolist()
        dataframe = dataframe.drop(index_video_data)

        dataframe = dataframe.sort_values(by="date")
        dataframe.drop_duplicates(subset=['text'], keep='first', inplace = True)

        java_noise_pattern = re.compile(r'(!function\().*(\(\);)', re.UNICODE)
        dataframe.text = dataframe.text.apply(lambda x: java_noise_pattern.sub('', x))

        japanese_characters_pattern = re.compile(
            r'([{\u3000-\u303F}{一-龯}\p{IsHira}\p{IsKatakana}]+)',
            re.UNICODE
        )
        dataframe.text = dataframe.text.apply(lambda x: japanese_characters_pattern.sub(' ', x))

        dataframe = self.remove_outliers_by_percentile(
            dataframe, text_size_lower_percentile, text_size_upper_percentile
        )

        return dataframe
    
