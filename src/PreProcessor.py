import numpy as np
import pandas as pd
import en_core_web_sm
import operator
from functools import reduce

nlp = en_core_web_sm.load()

class PreProcessor:

    def __init__(self, texts = pd.Series(), list_of_tokens = pd.Series()):
        self.texts = texts
        self.list_of_tokens = list_of_tokens

    def tokenize_single_text(self, text, include_stopwords=False, include_punctuation=False,
                         include_like_num=False, include_only_ascii=True, include_pos=[]):
        return [token for token in nlp(text) if all([not token.is_space,
                                                 not token.like_url,
                                                 token.is_ascii or not include_only_ascii,
                                                 include_stopwords or not token.is_stop,
                                                 include_punctuation or not token.is_punct,
                                                 include_like_num or not token.like_num,
                                                 include_pos == [] or token.pos_ in include_pos])]

    def tokenize_multiple_texts(self, texts, include_stopwords=False, include_punctuation=False,
                            include_like_num=False, include_only_ascii=True, include_pos=[]):
        docs = list(nlp.pipe(texts))
        doc_tokens = []
        for doc in docs:
            tokens = [token for token in doc if all([not token.is_space,
                                                    not token.like_url,
                                                    token.is_ascii or not include_only_ascii,
                                                    include_stopwords or not token.is_stop,
                                                    include_punctuation or not token.is_punct,
                                                    include_like_num or not token.like_num,
                                                    include_pos == [] or token.pos_ in include_pos])]
            doc_tokens.append(tokens)
        return doc_tokens

    def lemmatize_single_text(self, tokens):
        return [token.lemma_ for token in tokens]

    def lemmatize_multiple_texts(self, list_of_tokens):
        return [self.lemmatize_single_text(tokens) for tokens in list_of_tokens]

    def tokens_as_string(self, tokens):
        return [token.text for token in tokens]

    def list_of_tokens_as_string(self, list_of_tokens):
        return [self.tokens_as_string(tokens) for tokens in list_of_tokens]

    def lowercase_tokens(self, tokens):
        return [token.lower() for token in tokens]

    def lowercase_list_of_tokens(self, list_of_tokens):
        return [self.lowercase_tokens(tokens) for tokens in list_of_tokens]

    def flatten(self, list_of_tokens):
        return reduce(operator.iconcat, list_of_tokens, [])

