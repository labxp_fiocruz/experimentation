import pandas as pd
from utils import remove_special_characters, clean_broke_lines, default_data_filename

class DataLoader:

    def __init__(self):
        pass

    def get_dataframe(self, data_filename=default_data_filename):
        was_cleaned = clean_broke_lines(data_filename)
        dataframe = pd.read_csv(data_filename)

        if not was_cleaned:
            dataframe = self.mitigate_dirty_data(dataframe)

        return dataframe

    def mitigate_dirty_data(self, dataframe):
        colunas = list(dataframe.columns)
        clean_dataset = dataframe.copy()
        last_value = clean_dataset['_id'][2813]

        for i in range(1,len(colunas)):
            last_value = dataframe[colunas[i]][2813]
            clean_dataset[colunas[i]][2813] = dataframe[colunas[i-1]][2813]

        clean_dataset["_id"][2813] = dataframe["_id"][2812]
        clean_dataset["title"][2813] = dataframe["title"][2812]

        return clean_dataset
