
default_data_filename = '../data/news_data_final.csv'
default_train_filename = '../data/train.csv'
default_test_filename = '../data/test.csv'
default_output_filename = '../data/output.json'

def remove_special_characters(str_data_text):
    acceptable_special = """
    ,_().- '"/:

    """
    str_data_text_modified = []
    for i in range(len(str_data_text)):
        if str_data_text[i] in acceptable_special or str_data_text[i].isalpha() or str_data_text[i].isalnum():
            str_data_text_modified.append(str_data_text[i])
    return ''.join(str_data_text_modified).replace('\n,', ',')

def clean_broke_lines(data_filename=default_data_filename):
    modified_file_content = None
    try:
        with open(data_filename, 'r') as file:
            modified_file_content = remove_special_characters(file.read())
        with open(data_filename, 'w') as file:
            file.write(modified_file_content)

    except UnicodeDecodeError:
        return False

    return True

def order_dictionary(dictionary):
     return dict(sorted(dictionary.items(), key=lambda x: x[1], reverse = True))
